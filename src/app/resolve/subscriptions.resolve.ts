import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { SubscriptionsService } from '../services/subscriptions.service';
import { AuthService } from '../services/auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsResolve implements Resolve<any> {

  constructor(
    private subscriptionsService: SubscriptionsService,
    private authService: AuthService
  ) { }

  resolve(): Observable<any> {
    const userId = this.authService.loggedId;
    return combineLatest(
      this.subscriptionsService.getFollowings(userId),
      this.subscriptionsService.getFollowers(userId)
    ).pipe(map(
      (response) => ({
        followings: response[0].users,
        followers: response[1].users
      })
    ));
  }
}
