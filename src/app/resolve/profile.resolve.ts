import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ProfileService } from '../services/profile.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileResolve implements Resolve<any> {

  constructor(
    private profileService: ProfileService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any>  {
    return this.profileService.getProfile(+route.params.id);
  }
}
