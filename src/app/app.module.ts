import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatInputModule,
  MatSelectModule,
  MatRadioModule,
  MatCardModule,
  MatSnackBarModule,
  MatTabsModule,
  MatStepperModule,
  MatDialogModule
} from '@angular/material';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ExhibitComponent } from './components/exhibit/exhibit.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ProfilePreviewComponent } from './components/profile-preview/profile-preview.component';
import { HttpClientModule } from '@angular/common/http';
import { EmbedVideo } from 'ngx-embed-video';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { NewsFeedComponent } from './components/news-feed/news-feed.component';
import { ExhibitBuilderComponent } from './components/exhibit-builder/exhibit-builder.component';
import { ExpositionBuilderComponent } from './components/exposition-builder/exposition-builder.component';
import { ExpositionComponent } from './components/exposition/exposition.component';
import { GalleryModule } from  '@ngx-gallery/core';
import { LightboxModule } from  '@ngx-gallery/lightbox';
import { GallerizeModule } from  '@ngx-gallery/gallerize';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    SignInComponent,
    SignUpComponent,
    ExhibitComponent,
    ProfileComponent,
    ProfilePreviewComponent,
    SubscriptionsComponent,
    NewsFeedComponent,
    ExhibitBuilderComponent,
    ExpositionBuilderComponent,
    ExpositionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatSnackBarModule,
    MatTabsModule,
    MatStepperModule,
    MatDialogModule,
    ReactiveFormsModule,
    HttpClientModule,
    EmbedVideo.forRoot(),
    GalleryModule.withConfig({ loadingMode: 'indeterminate' }),
    LightboxModule,
    GallerizeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
