import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ProfileResolve } from './resolve/profile.resolve';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { SubscriptionsResolve } from './resolve/subscriptions.resolve';
import { NewsFeedComponent } from './components/news-feed/news-feed.component';
import { ExhibitBuilderComponent } from './components/exhibit-builder/exhibit-builder.component';
import { ExpositionBuilderComponent } from './components/exposition-builder/exposition-builder.component';

const routes: Routes = [
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  {
    path: 'user/:id',
    component: ProfileComponent,
    resolve: {
      profile: ProfileResolve,
      subscriptions: SubscriptionsResolve
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'subscriptions',
    component: SubscriptionsComponent,
    resolve: { subscriptions: SubscriptionsResolve },
    runGuardsAndResolvers: 'always'
  },
  { path: 'feed', component: NewsFeedComponent },
  { path: 'build/exhibit', component: ExhibitBuilderComponent },
  { path: 'build/exposition', component: ExpositionBuilderComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
