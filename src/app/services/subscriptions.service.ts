import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsService {

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) { }

  createSubscription(followerId: number, followingId: number): Observable<any> {
    return this.httpClient.post(
      environment.apiUrl + 'subscribe',
      { follower_id: followerId, following_id: followingId },
      { headers: this.authService.authorization }
    );
  }

  deleteSubscription(followerId: number, followingId: number): Observable<any> {
    return this.httpClient.post(
      environment.apiUrl + 'unsubscribe',
      { follower_id: followerId, following_id: followingId },
      { headers: this.authService.authorization }
    );
  }

  getFollowings(userId: number): Observable<any> {
    return this.httpClient
      .get(environment.apiUrl + 'following/' + userId);
  }

  getFollowers(userId: number): Observable<any> {
    return this.httpClient
      .get(environment.apiUrl + 'followers/' + userId);
  }
}
