import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsFeedService {

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) { }

  getFeed(): Observable<any> {
    return this.httpClient.get(
      environment.apiUrl + 'feed',
      { headers: this.authService.authorization }
    );
  }
}
