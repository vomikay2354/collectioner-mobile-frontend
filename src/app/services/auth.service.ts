import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  public get authorization(): HttpHeaders {
    const auth = `Bearer ${localStorage.token}`;
    return new HttpHeaders({ Authorization: auth });
  }

  public get loggedId(): number {
    return +localStorage.getItem('userId');
  }


  public get logged(): boolean {
    return !!localStorage.getItem('token');
  }

  signOut(): void {
    localStorage.clear();
  }

  signIn(email: string, password: string): Observable<any> {
    return this.httpClient.post(
      environment.apiUrl + 'login',
      { username: email, password }
    );
  }

  signUp(name: string, type: number, email: string, password: string): Observable<any> {
    return this.httpClient.post(
      `${environment.apiUrl}register`,
      { name, type, email, password }
    );
  }
}
