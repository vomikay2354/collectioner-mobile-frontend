import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) { }

  getUserExhibits(uid: number | string): Observable<any> {
    return this.httpClient.get(
      environment.apiUrl + 'exhibit_posts/account/' + uid,
      { headers: this.authService.authorization }
    );
  }

  createExhibit(title: string, description: string, imageSrc?: string, videoSrc?: string): Observable<any> {
    const type = 1;
    return this.httpClient.post(
      environment.apiUrl + 'create/post',
      { type, title, description, imageSrc, videoSrc },
      { headers: this.authService.authorization }
    );
  }

  createExposition(title: string, description: string, displayType: number, exibits: { id: number }[]): Observable<any> {
    const type = 2;
    return this.httpClient.post(
      environment.apiUrl + 'create/post',
      { type, title, description, display_type: displayType, exhibit_ids: exibits },
      { headers: this.authService.authorization }
    );
  }

  deletePost(id: number): Observable<any> {
    return this.httpClient.post(
      environment.apiUrl + 'delete/post', { id },
      { headers: this.authService.authorization }
    );
  }

  likePost(id: number): Observable<any> {
    return this.httpClient.post(
      environment.apiUrl + 'like',
      { user_id: this.authService.loggedId, post_id: id },
      { headers: this.authService.authorization }
    );
  }

  unlikePost(id: number): Observable<any> {
    return this.httpClient.post(
      environment.apiUrl + 'unlike',
      { user_id: this.authService.loggedId, post_id: id },
      { headers: this.authService.authorization }
    );
  }
}
