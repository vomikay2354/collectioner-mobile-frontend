import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  private config: MatSnackBarConfig = {
    duration: 2000,
    verticalPosition: 'bottom',
    horizontalPosition: 'center'
  };

  constructor(private snackBar: MatSnackBar) { }

  error(message: string): void {
    this.snackBar.open(message, 'OK', this.config);
  }

}
