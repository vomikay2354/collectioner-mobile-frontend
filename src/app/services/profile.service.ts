import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getProfile(userId: number): Observable<any> {
    return this.httpClient.get<any>(environment.apiUrl + 'account/' + userId);
  }

}
