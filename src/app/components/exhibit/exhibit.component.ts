import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { EmbedVideoService } from 'ngx-embed-video';
import { PostsService } from 'src/app/services/posts.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ErrorService } from 'src/app/services/error.service';

@Component({
  selector: 'app-exhibit',
  templateUrl: './exhibit.component.html',
  styleUrls: ['./exhibit.component.css']
})
export class ExhibitComponent implements OnInit {

  @Input() id: number;
  @Input() userId: number;
  @Input() userName: string;
  @Input() userType: number;
  @Input() title: string;
  @Input() description: string;
  @Input() imageSrc?: string;
  @Input() videoSrc?: string;
  @Input() like: any;
  @Input() banned: boolean;

  avatarSrc = '../../../assets/img/default_avatar.png';
  liked = false;

  image?: any;
  video?: any;

  constructor(
    private embedService: EmbedVideoService,
    private postsService: PostsService,
    private router: Router,
    private authService: AuthService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    if (this.imageSrc) {
      this.image = environment.apiUrl + this.imageSrc;
    }
    if (this.videoSrc) {
      this.video = this.embedService.embed(this.videoSrc);
    }
    this.liked = !!this.like.users.find(
      (user) => user === this.authService.loggedId);
  }

  onLike() {
    this.postsService
      .likePost(this.id)
      .subscribe(
        () => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        },
        (error) => this.errorService.error(error.message)
      );
  }

  onUnlike() {
    this.postsService
      .unlikePost(this.id)
      .subscribe(
        () => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        },
        (error) => this.errorService.error(error.message)
      );
  }
}
