import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';
import { GalleryItem, Gallery, ImageItem, ImageSize, ThumbnailsPosition, YoutubeItem } from '@ngx-gallery/core';
import { Router } from '@angular/router';
import { PostsService } from 'src/app/services/posts.service';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-exposition',
  templateUrl: './exposition.component.html',
  styleUrls: ['./exposition.component.css']
})
export class ExpositionComponent implements OnInit {

  @Input() id: number;
  @Input() userId: number;
  @Input() userName: string;
  @Input() userType: number;
  @Input() title: string;
  @Input() description: string;
  @Input() displayType: number;
  @Input() exhibits: any[];
  @Input() like: any;
  @Input() banned: boolean;

  avatarSrc = '../../../assets/img/default_avatar.png';
  liked = false;

  @ViewChild('itemTemplate')
  itemTemplate: TemplateRef<any>;

  items: GalleryItem[] = [];

  constructor(
    public gallery: Gallery,
    public authService: AuthService,
    private router: Router,
    private postsService: PostsService
  ) { }

  ngOnInit() {
    this.exhibits.map(exh => {
      let item: GalleryItem;
      if (exh.imageSrc) {
        item = new ImageItem({
          src: environment.apiUrl + exh.imageSrc,
          thumb: environment.apiUrl + exh.imageSrc,
          title: exh.title,
          description: exh.description
        });
      } else if (exh.videoSrc) {
        const params = new URL(exh.videoSrc).searchParams;
        const id = params.get('v');
        if (id) {
          item = new YoutubeItem({ src: id });
        }
      }
      this.items.push(item);
    });
    if (this.displayType === 1) {
      const lightboxGalleryRef = this.gallery.ref('gallery-' + this.id);
      lightboxGalleryRef.setConfig({
        imageSize: ImageSize.Cover,
        thumbPosition: ThumbnailsPosition.Top,
        itemTemplate: this.itemTemplate
      });
      lightboxGalleryRef.load(this.items);
    }
    this.liked = !!this.like.users.find(
      (user) => user === this.authService.loggedId);
  }

  onLike() {
    this.postsService
      .likePost(this.id)
      .subscribe(
        () => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        }
      );
  }

  onUnlike() {
    this.postsService
      .unlikePost(this.id)
      .subscribe(
        () => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        }
      );
  }

}
