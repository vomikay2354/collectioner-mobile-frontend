import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { PostsService } from 'src/app/services/posts.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { ErrorService } from 'src/app/services/error.service';
import { EmbedVideoService } from 'ngx-embed-video';

@Component({
  selector: 'app-exposition-builder',
  templateUrl: './exposition-builder.component.html',
  styleUrls: ['./exposition-builder.component.css']
})
export class ExpositionBuilderComponent implements OnInit {

  form: FormGroup;
  exhibits$: Observable<any>;
  api = environment.apiUrl;

  constructor(
    public embedService: EmbedVideoService,
    private router: Router,
    private renderer: Renderer2,
    private fb: FormBuilder,
    private authService: AuthService,
    private postsService: PostsService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      title: this.fb.control('', Validators.required),
      description: this.fb.control('', Validators.required),
      displayType: this.fb.control('', Validators.required),
      exhibits: this.fb.array([], Validators.required)
    });
    const uid = this.authService.loggedId;
    this.exhibits$ = this.postsService.getUserExhibits(uid);
  }

  onSubmit() {
    this.postsService
      .createExposition(
        this.form.controls.title.value,
        this.form.controls.description.value,
        this.form.controls.displayType.value,
        this.form.controls.exhibits.value
      )
      .subscribe(
        () => {
          const uid = this.authService.loggedId;
          this.router.navigate([`/user/${uid}`]);
        },
        (error) => this.errorService.error(error.message)
      );
  }

  onClick(el: any, id: number) {
    const exhibits = this.form.controls.exhibits as FormArray;
    const predicat = (exhibit) => (exhibit.id == id);
    const index = exhibits.value.findIndex(predicat);
    if (index === -1) {
      exhibits.push(this.fb.group({ id: id }));
      this.renderer.addClass(el, 'active');
    } else {
      exhibits.removeAt(index);
      this.renderer.removeClass(el, 'active');
    }
  }

}
