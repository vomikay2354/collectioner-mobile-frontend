import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { SubscriptionsService } from 'src/app/services/subscriptions.service';
import { ErrorService } from 'src/app/services/error.service';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnDestroy {

  navigationSubscription: Subscription;

  profile: any;
  avatarSrc = '../../../assets/img/default_avatar.png';
  isFollowing = false;

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private subscriptionsService: SubscriptionsService,
    private errorService: ErrorService,
    private postsService: PostsService
  ) {
    this.navigationSubscription = this.router.events.subscribe(
      (event) => {
        if (event instanceof NavigationEnd) {
          this.profile = this.route.snapshot.data.profile;
          const followings = this.route.snapshot.data.subscriptions.followings;
          this.isFollowing = !!followings.find(
            (following) => following.id === this.profile.info.id);
        }
      }
    );
  }

  ngOnDestroy() {
    this.navigationSubscription.unsubscribe();
  }

  onSubscribe() {
    const followerId = this.authService.loggedId;
    const followingId = this.profile.info.id;
    this.subscriptionsService
      .createSubscription(followerId, followingId)
      .subscribe(
        () => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        },
        (error) => this.errorService.error(error.message)
      );
  }

  onUnsubscribe() {
    const followerId = this.authService.loggedId;
    const followingId = this.profile.info.id;
    this.subscriptionsService
      .deleteSubscription(followerId, followingId)
      .subscribe(
        () => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        },
        (error) => this.errorService.error(error.message)
      );
  }

  onDeletePost(postId: number) {
    this.postsService
      .deletePost(postId)
      .subscribe(
        () => {
          this.router.navigated = false;
          this.router.navigate([this.router.url]);
        },
        (error) => this.errorService.error(error.message)
      );
  }
}
