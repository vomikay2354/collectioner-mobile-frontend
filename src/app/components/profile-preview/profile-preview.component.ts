import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-preview',
  templateUrl: './profile-preview.component.html',
  styleUrls: ['./profile-preview.component.css']
})
export class ProfilePreviewComponent implements OnInit {

  @Input() userId: number;
  @Input() userName: string;
  @Input() userType: number;

  avatarSrc = '../../../assets/img/default_avatar.png';

  constructor() { }

  ngOnInit() { }
}
