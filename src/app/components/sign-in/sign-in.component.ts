import { Component, ViewChild, TemplateRef } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ErrorService } from 'src/app/services/error.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {

  @ViewChild('dialog')
  dialogTemplate: TemplateRef<any>;

  form: FormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  constructor(
    private authService: AuthService,
    private errorService: ErrorService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  hasError(control: string, error: string): boolean {
    return (this.form.dirty || this.form.touched)
      && this.form.invalid
      && this.form.get(control).hasError(error);
  }

  onSubmit() {
    this.authService
      .signIn(
        this.form.controls.email.value,
        this.form.controls.password.value)
      .subscribe(
        (response) => {
          if (response.banned) {
            this.dialog.open(this.dialogTemplate, { width: '400px' });
            return;
          }
          localStorage.setItem('token', response.token);
          localStorage.setItem('userId', response.userId);
          this.router.navigate([`/user/${response.userId}`]);
        },
        (error) => this.errorService.error(error.message)
      );
  }

}
