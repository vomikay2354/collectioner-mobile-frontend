import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ErrorService } from 'src/app/services/error.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  form: FormGroup = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(64),
      Validators.pattern(/[ЁёА-Яа-яA-Za-z0-9 ]*/u)
    ]),
    type: new FormControl('', [
      Validators.required
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(32),
      Validators.pattern(/[\[\]\\!@#$%^&*()_=+{};:"|,.A-Za-z0-9]*/)
    ])
  });

  constructor(
    private authService: AuthService,
    private router: Router,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
  }

  hasError(control: string, error: string): boolean {
    return (this.form.dirty || this.form.touched)
      && this.form.invalid
      && this.form.get(control).hasError(error);
  }

  onSubmit() {
    this.authService
      .signUp(
        this.form.controls.name.value,
        this.form.controls.type.value,
        this.form.controls.email.value,
        this.form.controls.password.value)
      .subscribe(
        () => this.router.navigate(['/sign-in']),
        (error) => this.errorService.error(error.message)
      );
  }

}
