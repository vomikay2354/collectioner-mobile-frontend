import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { NewsFeedService } from 'src/app/services/news-feed.service';

@Component({
  selector: 'app-news-feed',
  templateUrl: './news-feed.component.html',
  styleUrls: ['./news-feed.component.css']
})
export class NewsFeedComponent implements OnInit, OnDestroy {

  feedSubscription: Subscription;
  updateTime = 2500;
  posts = [];

  constructor(private newsFeedService: NewsFeedService) { }

  ngOnInit(): void {
    this.feedSubscription = interval(this.updateTime)
      .pipe(startWith(0), switchMap(() => this.newsFeedService.getFeed()))
      .subscribe(
        (posts) => {
          const first = JSON.stringify(posts);
          const second = JSON.stringify(this.posts);
          if (first !== second) {
            this.posts = posts;
          }
        }
      );
  }

  ngOnDestroy(): void {
    this.feedSubscription.unsubscribe();
  }
}
