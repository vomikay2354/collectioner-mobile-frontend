import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnDestroy {

  navigationSubscription: Subscription;
  subscriptions: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.navigationSubscription = this.router.events.subscribe(
      (event) => {
        if (event instanceof NavigationEnd) {
          this.subscriptions = this.route.snapshot.data.subscriptions;
        }
      }
    );
  }

  ngOnDestroy() {
    this.navigationSubscription.unsubscribe();
  }
}
