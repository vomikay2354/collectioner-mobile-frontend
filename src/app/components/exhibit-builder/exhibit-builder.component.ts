import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { PostsService } from 'src/app/services/posts.service';
import { ErrorService } from 'src/app/services/error.service';

@Component({
  selector: 'app-exhibit-builder',
  templateUrl: './exhibit-builder.component.html',
  styleUrls: ['./exhibit-builder.component.css']
})
export class ExhibitBuilderComponent {

  form: FormGroup = new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    imageSrc: new FormControl('', Validators.required),
    videoSrc: new FormControl('')
  });

  constructor(
    private router: Router,
    private authService: AuthService,
    private postsService: PostsService,
    private errorService: ErrorService
  ) { }

  hasError(control: string, error: string): boolean {
    return (this.form.dirty || this.form.touched)
      && this.form.invalid
      && this.form.get(control).hasError(error);
  }

  onChangeImage(event: Event | any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.form.controls.imageSrc.setValue(reader.result);
    };
  }

  onCreate() {
    this.postsService
      .createExhibit(
        this.form.controls.title.value,
        this.form.controls.description.value,
        this.form.controls.imageSrc.value,
        this.form.controls.videoSrc.value
      )
      .subscribe(
        () => {
          const userId = this.authService.loggedId;
          this.router.navigate(['/user/' + userId]);
        },
        (error) => this.errorService.error(error.message)
      );
  }

  onCancel() {
    const userId = this.authService.loggedId;
    this.router.navigate(['/user/' + userId]);
  }
}
